-- users
drop table if exists `user`;
create table `user`(
  id integer primary key autoincrement,
  name text not null unique,
  pass text not null,
  active integer
);

-- groups
drop table if exists `group`;
create table `group`(
    name text primary key,
    label text not null
);

drop table if exists `user_groups`;
create table `user_groups`(
  uid integer not null,
  gname text not null,
  primary key (uid, gname)
);

-- permissions
drop table if exists `permission`;
create table `permission`(
  name text primary key,
  label text not null
);

drop table if exists `user_permissions`;
create table `user_permissions`(
  uid integer not null,
  permission text not null,
  value integer not null, -- -1 = DENY, 0 = DONTCARE, 1 = ALLOW
  primary key (uid, permission)
);

drop table if exists `group_permissions`;
create table `group_permissions`(
  gname text not null,
  permission text not null,
  value integer not null, -- -1 = DENY, 0 = DONTCARE, 1 = ALLOW
  primary key (gname, permission)
);

-- mail, xmpp, website, twitter etcpp, associated to users
--drop table if exists `contact`;
--create table `contact` (
--  uid integer not null,
--  type text not null,
--  value text not null
--);

-- content
--drop table if exists `content`;
--create table `content`(
--    id integer primary key autoincrement,
--    owner integer not null,
--    created integer not null,
--    updated integer not null,
--    title text not null,
--    body text not null
--);
--
---- categories
--drop table if exists `categories`;
--create table `categories`(
--    name text primary key,
--    label text not null
--);
--
--drop table if exists `content_category`;
--create table `content_category`(
--  cid integer primary key,
--  category text not null
--);
--
---- tags
--drop table if exists `tags`;
--create table `tags`(
--  tag text primary key,
--  label text not null
--);
--
--drop table if exists `content_tags`;
--create table `content_tags`(
--  cid integer not null,
--  tag text not null,
--  primary key (cid, tag)
--);
--
----meta
--drop table if exists `meta`;
--create table `meta`(
--  cid integer not null,
--  type text not null,
--  value text not null,
--  primary key (cid, type)
--);
--
---- media
--drop table if exists `media`;
--create table `media`(
--    cid integer not null,
--    type text not null,
--    path text not null
--);
--
--
---- comments
--
--drop table if exists `comments`;
--create table `comments`(
--  id integer primary key autoincrement,
--  cid integer not null,
--  name text not null,
--  contact text,
--  link text,
--  title text not null,
--  body text not null
--);
--
---- challenges
--drop table if exists `challenges`;
--create table `challenges`(
--  id integer primary key autoincrement,
--  created integer not null,
--  title text not null,
--  description text not null
--);
--
--drop table if exists `challenge_users`;
--create table `challenge_users`(
--    cid integer not null,
--    uid integer not null,
--    primary key (cid, uid)
--);
--
--drop table if exists `challenge_days`;
--create table `challenge_days`(
--  cid integer not null,
--  uid integer not null,
--  day integer not null,
--  fulfilled integer not null,
--  primary key (cid, uid, day)
--);
--
---- availability
--drop table if exists `availability`;
--create table `availability`(
--  uid integer not null,
--  created integer not null,
--  level integer not null,
--  comment text,
--  primary key (uid, created)
--);
