"""
Common module.

This module holds all core functionality that has to be in the same module.
Might get merged into the skunkfart main module.

..  todo::

    Check if automatic permission creation for children of ``Administered``
    is a feasible approach, if so: implement that shit.
"""

import random, string

from functools import wraps
from hashlib import sha512
from collections import OrderedDict as odict
from werkzeug.datastructures import MultiDict
from flask.ext.wtf import Form, FormField, TextField, PasswordField, RadioField, SubmitField, SelectField, SelectMultipleField, BooleanField, Required, EqualTo
from flask import g, request, session, flash, redirect, url_for, abort
from mod import implements, invoke
from skunkfart import app, db_query, db_commit, view, render, form_process, block_set, content_set, title_append, modules
from fields import RangeField
from elements import value, container, text, link, table, form, ulist

administered = odict()
form_constructors = {}
form_handlers = {}
field_renderers = {}


# Autoadmin related classes after this

class Field(object):

    """
    Class to denote a field handled by the automatic administration interface.
    """

    def __init__(self, val, key = None, form = None, handler = None, label = '', renderer = None, multi = False):

        self.val = val
        self.key = key if key else type(val)
        self.form = form
        self.handler = handler
        self.label = label
        self.renderer = renderer
        self.multi = multi

        self.parent_id = None


    def getform(self):

        """
        Obtain a fields' form constructor.

        A fields' form constructor is either the form constructor associated to the
        type of the default value of the field (``form_constructors[fieldtype.__name__]``)
        or a custom constructor (``field.form``).

        ..  note::

            If no fitting form constructor is found, this function will throw a
            LoopupError exception.

        """

        if callable(self.form):
            return self.form

        if form_constructors.has_key(self.key) and \
        ((self.multi and form_constructors[self.key].multi) or not self.multi):
            return form_constructors[self.key]

        msg = 'No multi form field constructor for key: %s' if self.multi else 'No form field constructor for key: %s'

        raise LookupError(msg % (str(self.key)))


    def render(self):

        if callable(self.renderer):
            return self.renderer(self)

        if field_renderers.has_key(self.key) and \
        ((self.multi and field_renderers[self.key].multi) or not self.multi):
            return field_renderers[self.key](self)

        msg = 'No multi field renderer for key: %s' if self.multi else 'No field renderer for key: %s'

        raise LookupError(msg % (str(self.key)))



class _AdminMeta(type):

    """
    Metaclass for all ``Administered`` classes.

    Handles properly filling the __fields__ property of ``Administered`` classes.
    """

    def __new__(cls, name, parents, props):

        fields = odict()

        modname = props['__module__'][8:]

        for n, v in props.items():
            if isinstance(v, Field):
                props[n] = v.val
                fields[n] = v

        props['__fields__'] = fields

        clsobj = super(_AdminMeta, cls).__new__(cls, name, parents, props)

        if not name is 'Administered':

            if not administered.has_key(modname):
                administered[modname] = {}

            administered[modname][name] = clsobj

        return clsobj



class Administered(object):
    """
    Base class for the automatically created administration interface.

    Any class derived from this one will have its administration interface
    automagically created at /admin/<mod>/<cls>, which will feature add, edit
    and delete functionality.
    """
    __metaclass__ = _AdminMeta

    
    def __new__(cls, *args, **kwargs):

        if kwargs.has_key('id'):
            id = kwargs['id']
        elif len(args) > 0 and type(args[0]) is int:
            id = args[0]
        else:
            id = None

        fields = odict()

        for k, field in cls.__fields__.iteritems():
            fields[k] = Field(field.val, field.key, field.form, field.handler, field.label, field.renderer, field.multi)

            if type(id) is int:
                fields[k].parent_id = id

        obj = super(Administered, cls).__new__(cls, *args, **kwargs)
        obj.__fields__ = fields

        return obj


    def __setattr__(self, name, value):

        if self.__fields__.has_key(name):
            self.__fields__[name].val = value

        super(Administered, self).__setattr__(name, value)


    def getform(self):

        """
        Create the administration (add / edit) form for an ``Administered`` class.
        """

        class FormAdmin(Form):
            pass

        try:

            for (n, field) in self.__fields__.iteritems():
                constructor = field.getform()
                if hasattr(field, 'label'):
                    name = unicode(field.label)
                else:
                    name = n

                setattr(FormAdmin, n, constructor(name))

            setattr(FormAdmin, 'submit', SubmitField('Save'))

            return FormAdmin

        except: raise


    def identity(self):

        if hasattr(self, 'name'):
            return str(self.name)

        return str(self.id)



# administratable classes

class Permission(Administered):

    """
    The permission class.

    Any user and group can have an arbitrary number of permissions associated,
    each holding a *value* property of either *True* or *False*, denoting 
    whether access is granted or denied.

    """  

    _id = 'name'

    name = Field('', label='Permission name', renderer=lambda x: curry(render_name, 'common', 'User')(x)) # internal name
    label = Field('') # human readable name
    value = False # Access value, False = DENY, True = ALLOW
    in_db = False # True if permission exists in db

    def __init__(self, name = None, user = None, group = None):

        """
        Load an existing permission or create a blank permission object.

        If *name* is given, load the corresponding permission, otherwise
        create a blank permission object.

        :param name: The name of the permission to load.
        :param user: user object; This correctly fills the *value* property for the denoted user.
        :param group: group object; This correctly fills the *value* property for the denoted group.

        ..  todo::

            Check if function behaves correctly when called with user not yet in database.
        """

        if name:
            
            p = db_query('SELECT * FROM `permission` WHERE name = ?',
              [name], True)

            if p is None:
                raise LookupError(('No such permission: %s' % (name)))

            self.name = p['name']
            self.label = p['label']
            self.in_db = True

            p = None

            if user:
                p = db_query('SELECT value FROM `user_permissions` WHERE uid = ? AND permission = ?',
                  [user.id, name], True)

            elif group:
                p = db_query('SELECT value FROM `group_permissions` WHERE gname = ? AND permission = ?',
                    [group.name, name], True)

            if p:
                self.value = p['value'] is 1
            else:
                self.value = False


    def save(self):

        """
        Save the current permission to the database.

        If the permission already exists, it will be updated, if it doesn't it 
        will be inserted. To dertermine whether to update or insert, the 
        function checks if self.in_db evaluates to True.

        ..  note::
            The access value (grant/deny access) of a permission associated to
            a user or group is not saved when saving a permission, but when
            saving that user or group.
        """

        if not self.in_db:

            res = g.dbcon.execute('INSERT INTO `permission` (name, label) VALUES (?, ?)',
                [self.name, self.label])
            g.dbcon.commit()
            self.in_db = True

        else:
            
            res = g.dbcon.execute('UPDATE permission SET label = ? WHERE name = ?',
                [self.label, self.name])
            g.dbcon.commit()

        return res


    def delete(self):

        """
        Delete a permission.

        ..  todo::
            This function will probably fail if called for a non-existant
            permission. Check if this is the case, and throw an exception if
            need be.
        """

        g.dbcon.execute('DELETE FROM group_permissions WHERE permission = ?', [self.name])
        g.dbcon.commit()

        g.dbcon.execute('DELETE FROM user_permissions WHERE permission = ?', [self.name])

        res = g.dbcon.execute('DELETE FROM permission WHERE name = ?',
            [self.name])

        g.dbcon.commit()

        self.name = None
        self.label = None
        self.value = None
        self.in_db = None

        return res



class Group(Administered):

    """
    The group class.

    Any user can be associated to an arbitrary number of groups whiches
    permission it inherits.

    ..  todo::
        This class is still missing a *delete* method.
    """

    _id = 'name'

    name = Field('', label='Name') #internal name
    label = Field('', label='Label') #human readable name
    perms = Field(Permission(), label='Permissions', multi=True) # dict of perms
    in_db = False # True if group exists in db

    def __init__(self, name = None):

        """
        Load an existing group or create a blank group object.

        :param name: The name of the group to load.
        """

        if name:
            g = db_query('SELECT * FROM `group` WHERE name = ?',
              [name], True)
            
            if g is None:
                raise LookupError(('No such group: %s' % (name)))

            self.name = g['name']
            self.label = g['label']
            self.in_db = True

            self.perms = {}


            self.perms_load()


    def save(self):

        """
        Save the current group to the database.

        If the group already exists, it will be updated, if it doesn't, it will
        be inserted.

        To determine whether to update or insert, the function checks if 
        *self.in_db* evaluates to True.

        ..  todo::

            If group is not yet in db, check if a group with same name already
            exists in order to avoid data consistency problems.
        """

        if not self.in_db:

            res = g.dbcon.execute('INSERT INTO `group` (name, label) VALUES (?, ?)',
                [self.name, self.label])
            g.dbcon.commit()
            self.in_db = True

        else:
            
            res = g.dbcon.execute('UPDATE `group` SET label = ? WHERE name = ?',
                [self.label, self.name])
            g.dbcon.commit()

        self.perms_update()

        return res

  
    def delete(self):

        """
        Delete the current group.

        If the group exists in the database, delete all info and associations
        for it.

        ..  todo::

            Test this function.
        """

        if self.in_db:

            resg = g.dbcon.execute('DELETE FROM group WHERE name = ?',
                [self.name])
            g.dbcon.commit()

            resu = g.dbcon.execute('DELETE FROM user_groups WHERE gname = ?',
                [self.name])
            g.dbcon.commit()

            resp = g.dbcon.execute('DELETE FROM group_permissions WHERE gname = ?',
                [self.name])
            g.dbcon.commit()

            return resg and resu and resp

        raise LookupError("Can't delete group: Not in database.")



    def perms_load(self):

        """
        Load the current permissions for this group.

        This function loads the permissions associated to this group from the
        database and puts them into the *perms* dict of this group object.

        ..  todo::

            Check if function call fails when called for a user not yet in the
            database.
        """

        rows = db_query('SELECT `permission` FROM group_permissions WHERE gname = ?',
            [self.name])

        for row in rows:
          self.perms[row['permission']] = Permission(row['permission'], group=self)

          
    def perms_update(self):

        """
        Update this groups permission records in the database.

        This function writes the permissions currently associated to this group
        object into the database.

        ..  todo::
        
            This function will fail if the group does not exist in the database.
            Check if in_db and throw an exception when needed.
        """

        res = g.dbcon.execute('DELETE FROM group_permissions WHERE gname = ?',
            [self.name])
        g.dbcon.commit()

        for perm in self.perms.values():

            value = 1 if perm.value is True else 0

            g.dbcon.execute('INSERT INTO `group_permissions` (gname, permission, value) VALUES (?, ?, ?)',
                [self.name, perm.name, value])
            g.dbcon.commit()



class User(Administered):

    """
    The user class

    Used for authorizing and authenticating the different users/visitors of the
    site, including the 'Anon' user which is used for visitors that are not
    logged in.

    Uses automated administration interface creation.
    
    
    .. todo::
        Throw an exception when trying to change id
        Or rather, create a new user if that happens.

    """

    id = None # user id
    owner = id
    name = Field('', label='Username', renderer=lambda x: curry(render_name, 'common', 'User')(x)) # username
    passhash = None # password hash
    password = Field('', label=u'Password', form=lambda x: PasswordField(x), renderer=lambda x: False) # plain text password (for adding/updating, normally None)
    active = Field(False, label=u'Active') # Account active?
    groups = Field(Group(), label='Groups', multi=True) # dict of group objects
    perms = Field(Permission(), label='Permissions', multi=True) # dict of perms
    authmap = None # hashmap of all existing permissions and their final value for authorization checks
    in_db = False # True if user exists in db


    def __init__(self, id = None, name = None):
        """
        Load an existing user or create a blank user object.

        If id or name are given, load the corresponding user
        otherwise, create a blank user object.

        :param id:   The id of the user to load
        :param name: The name of the user to load
        :rtype: None
        """
  
        if id is None and name is None:

            self.id = None
            self.name = ''
            self.groups = {}
            self.perms = {}
            self.authmap = {}
            self.password = ''
            self.in_db = False

            self.perms_load()
            self.groups_load()
            self.authmap_build()
            return

        elif name:

            u = db_query('SELECT * FROM `user` WHERE name = ?',
              [name], True)
        else:

            u = db_query('SELECT * FROM `user` WHERE id = ?',
              [id], True)

        if u:

            self.id = int(u['id'])
            self.name = u['name']
            self.passhash = u['pass']
            self.password = ''
            self.active = bool(u['active'])
            self.groups = {}
            self.perms = {}
            self.authmap = {}
            self.in_db = True

            self.perms_load()
            self.groups_load()
            self.authmap_build()


        else:

            raise LookupError('No such user')

        self.owner = self.id


    def save(self):
        
        """
        Save the current user object to the database.

        If the user already exists, it will be updated, if it doesn't,
        it will be created.

        To determine whether to update or insert, the function checks if
        self.id is set to something other than None.

        ..  todo::
            
            If user is not yet in db, check if a user with same name already
            exists in order to avoid data consistency problems.
        """

        if not self.in_db:
    
            hash = sha512(self.name+self.password).hexdigest() if self.password else self.passhash
            active = 1 if self.active is True else 0

            self.passhash = hash
            self.password = None

            if self.id is not None:

                # check if user with this id already exists.
                count = db_query('SELECT COUNT(*) AS count FROM `user` WHERE id = ?', [self.id])[0]['count']

                if count == 0:
                    res = g.dbcon.execute('INSERT INTO `user` (id, name, pass, active) VALUES (?, ?, ?, ?)',
                        [self.id, self.name, hash, active])
                    g.dbcon.commit()
                    self.id = res.lastrowid
                    self.owner = self.id

                else:
                    raise KeyError("A user with this id already exists:  %d" % (self.id, ))

            else:
                
                # check if user with this name already exists.
                count = db_query('SELECT COUNT(*) AS count FROM `user` WHERE name = ?', [self.name])[0]['count']

                if count == 0:
                    res = g.dbcon.execute('INSERT INTO `user` (name, pass, active) VALUES (?, ?, ?)',
                        [self.name, hash, active])
                    g.dbcon.commit()
                    self.id = res.lastrowid
                    self.owner = self.id

                else:
                    raise KeyError("A user with this name already exists: %s" %(self.name, ))

        else:

            res = g.dbcon.execute('UPDATE user SET name = ?, pass = ?, active = ? WHERE id = ?',
                [self.name, self.passhash, self.active, self.id])
            g.dbcon.commit()
            self.in_db = True

        self.perms_update()
        self.groups_update()
        self.authmap_build()

        return res


    def delete(self):

        """
        Delete a user

        ..  todo::
            This function will probably fail if called for a non-existant
            user. Check if this is the case, and throw an exception if
            need be.
        """
        res = g.dbcon.execute('DELETE FROM user WHERE id = ?',
          [self.id])

        g.dbcon.commit()
        
        self.id = None
        self.name = None
        self.passhash = None
        self.active = None
        self.groups = None
        self.perms = None
        self.in_db = None

        return res


    def perms_load(self):

        """
        Load the current permissions for this user.

        This function loads the permissions directly associated to this user
        from the database and puts them into the *perms* dict of this user object.

        ..  todo::

            Check if function call fails when called for a user not yet in the
            database.
        """


        rows = db_query('SELECT permission FROM `user_permissions` WHERE uid = ?',
          [self.id])
        
        for row in rows:
            self.perms[row['permission']] = Permission(row['permission'], user=self)


    def perms_update(self):

        """
        Update this users permission records in the database.

        This function writes the permissions currently associated to this
        user object into the database.

        ..  todo::

            This function will fail if the user does not exist in the database.
            Check if in_db and throw an exception when needed.
        """

        res = g.dbcon.execute('DELETE FROM user_permissions WHERE uid = ?',
            [self.id])
        g.dbcon.commit()

        for perm in self.perms.values():

            value = 1 if perm.value is True else -1

            g.dbcon.execute('INSERT INTO `user_permissions` (uid, permission, value) VALUES (?, ?, ?)',
                [self.id, perm.name, value])
            g.dbcon.commit()


    def groups_load(self):

        """
        Load the groups this user is currently associated to.

        This function loads the group-associations of this user from the
        database and puts them into this user objects *groups* dict.

        ..  todo::

            Check if this function behaves correctly if the current user
            is not yet been inserted into the database.
        """

        rows = db_query('SELECT g.name FROM `group` g LEFT JOIN `user_groups` ug ON g.name = ug.gname WHERE ug.uid = ?',
          [self.id])

        for row in rows:
            self.groups[row['name']] = Group(row['name'])


    def groups_update(self):

        """
        Update the group-associations of this user in the database.

        This function writes the current group-associations of this user
        object into the database.

        ..  todo::

            This function will fail of the user does not exist in the database.
            Throw an exception when needed.
        """

        res = g.dbcon.execute('DELETE FROM user_groups WHERE uid = ?',
            [self.id])
        g.dbcon.commit()

        for group in self.groups.values():
            g.dbcon.execute('INSERT INTO `user_groups` (uid, gname) VALUES (?, ?)',
                [self.id, group.name])
            g.dbcon.commit()


    def authmap_build(self):

        """
        Build the authorization map for this user.

        This function builds the authorization map for this user object based
        on its *perms* and *groups* properties.

        The authmap is a dictionary holding the users' access status for every
        single permission, denoted by either True (granted) or False (denied).

        The authmap is created as this user objects *authmap* property.
        """

        self.authmap = {}

        p = perms_all()
        
        for perm in p.keys():
            self.authmap[perm] = False

        for group in self.groups.values():
            for perm in group.perms.values():
                self.authmap[perm.name] = perm.value
                #one false overrides all trues
                if perm.value is False:
                    break

 
        for key, perm in self.perms.iteritems():
            #but user-associated permissions override any group-permissions
            self.authmap[key] = perm.value


    def auth_granted(self, perm, *args, **kwargs):

        """
        Check if a certain permission is granted to this user.

        :param perm: The name of the permission to check for
        :rtype: bool
        """

        if self.authmap.has_key(perm):
            return self.authmap[perm]
        else:
            if app.config['DEBUG']:
                raise LookupError(('No such permission: %s' % (perm)))
            flash(('Failed to authenticate because permission does not exist: %s' % (perm)))
            return False


    def access(self, *args, **kwargs):

        """
        Perform a permission check with the possibility of a custom callback.

        This function calls the callback supplied in ``**kwargs['callback']``
        if any, or self.auth_granted as a fallback.

        This mechanism exists in order to enable custom callbacks for the
        *@access* decorator which is used to authorize a user to see a page.
        """

        if kwargs.has_key('callback'):
            args = list(args)
            args.insert(0, self)
            callback = kwargs['callback']
            del kwargs['callback']

        else:
            callback = self.auth_granted

        return callback(*args, **kwargs)
      



# custom form fields below this

class PermissionField(RangeField):

    def _value(self, *args, **kwargs):
        if type(self.data) is Permission:
            return {True: 1, False: -1}[self.data.value]

        return 0



class GroupField(SelectMultipleField):

    def __init__(self, label='', validators=None, **kwargs):
        super(GroupField, self).__init__(label, validators, **kwargs)



# decorators below this
 
def register_form_constructor(formkey, multi = False):

    """
    Decorator; Register a callback to a form constructor for a specific type (``formkey``).

    Example::

        @register_form_constructor(str)
        def element_string(name):
            return TextField(name)

    """

    def decorator(func):

        func.multi = multi
        if not form_constructors.has_key(formkey):
            form_constructors[formkey] = func

        else:
            raise KeyError('Formkey constructor already exists: '+str(formkey))


        return func

    return decorator


def register_form_handler(formkey, multi = False):

    """
    Decorator; Register a callback to a form handler for a specific type (``formkey``).
    """

    def decorator(func):
       
        func.multi = multi
        if not form_handlers.has_key(formkey):
            form_handlers[formkey] = func

        else:
            raise KeyError('Formkey handler already exists: '+str(formkey))

        return func

    return decorator


def register_field_renderer(key, multi = False):

    """
    Decorator; Register a callback
    """

    def decorator(func):

        func.multi = multi
        if not field_renderers.has_key(key):
            field_renderers[key] = func

        else:
            raise KeyError('Field rendered already exists: '+str(key))

        return func

    return decorator


def access(*args, **kwargs):

    """
    Decorator; Sets the permission needed to access this page.

    Alternatively, a custom access callback returning True or False might be
    passed.


    Example, using a permission name to determine access rights::

        @app.route('/foo')
        @access('access_foo')
        @view
        def page_foo():
            return value('Here be page content.')


    Example, using a custom access callback to determine access rights::

        def access_anon(user):
            if(user.id == 0):
                return True
            return False

        @app.route('/only/for/anon')
        @access(callback=access_anon)
        @view
        def page_anon():
            return (value('Only anon visitors get access to this.')
      

    ..  warning::
        
        This decorator has to be the below the app.route decorator for the page callback.


    ..  todo::
        
        Check if the custom callback example actually works
    """

    def decorator(func):

        @wraps(func)
        def c(*a, **kw):

            params = {'args': a, 'kwargs': kw}

            kwargs['params'] = params


            if g.user.access(*args, **kwargs):
                return func(*a, **kw)
            else:
                return abort(401, "Not authorized for access.")        
        return c

    return decorator


# access callbacks below this

def access_admin(user, mode, *args, **kwargs):

    kw = kwargs['params']['kwargs']

    try:

        mod = modules[kw['mod']] #TODO: Try Except
        cls = getattr(mod, kw['cls']) # TODO: see above

    except Exception as e:
        abort(500, ('Failed to determine access. %s' % (e.message)))

    if mode == 'add':
        return user.auth_granted("%s:%s" % (kw['cls'], mode))

    else:

        id = kw['id']
        obj = cls(id)

        if hasattr(obj, 'owner'): #object is owned by a user

            perm = "%s:%s_%s" % (kw['cls'], mode, 'own')

            if user.auth_granted(perm) and user.id == obj.owner:
                return True

        if hasattr(obj, 'group'): #object is owned by a group

            perm = "%s:%s_%s" % (kw['cls'], mode, 'group')
            if user.auth_granted(perm) and obj.group in user.groups.keys():
                return True 

        perm = "%s:%s_%s" % (kw['cls'], mode, 'all')
        return user.auth_granted(perm)



# auxiliary functions below this

def groups_list():

    """
    Get a list of all known Groups.
    """

    groups = odict()

    res = db_query('SELECT * FROM `group`')

    for row in res:
        groups[row['name']] = row['label']

    return groups


def perms_all():

    """
    Get a list of all known permissions.
    """

    perms = odict()
    res = db_query('SELECT name FROM `permission`')
    for row in res:
        perms[row['name']] = Permission(row['name'])
    return perms
    

def login(name, password):
    u = db_query('SELECT id, active FROM `user` WHERE name = ? AND pass = ?',
      [name, sha512(name+password).hexdigest()], True)

    if u:
        if u['active'] is 1:
            session['uid'] = u['id']
            invoke('user_load_current')

    return g.user


def curry(func, *arg, **kwarg):

    def call(*args, **kwargs):

        a = list(arg) #create a local copy rather than keeping a reference
        kw = dict(kwarg)
        args = list(args)
        while len(a):
            args.insert(0, a.pop())

        for k, v in kw.iteritems():
            if not kwargs.has_key(k): #only add kwarg if it's not already set, allows for overriding curried kwargs
                kwargs[k] = v

        return func(*args, **kwargs)

    return call


# hook implementations below this

@implements('permission_build')
def autoadmin_perms():

    perms = {}

    perms['admin:view'] = Permission()
    perms['admin:view'].name = 'admin_view'
    perms['admin:view'].label= 'View administration interface'

    admin_perms = odict()
    admin_perms[':add'] = 'Add %s'
    admin_perms[':view_all'] = 'View all %s'
    admin_perms[':view_group'] = 'View group %s'
    admin_perms[':view_own'] = 'View own %s'
    admin_perms[':edit_all'] = 'Edit all %s'
    admin_perms[':edit_group'] = 'Edit group %s'
    admin_perms[':edit_own'] = 'Edit own %s'
    admin_perms[':delete_all'] = 'Delete all %s'
    admin_perms[':delete_group'] = 'Delete group %s'
    admin_perms[':delete_own'] = 'Delete own %s'

    for mod, classes in administered.iteritems():
        for name in classes.keys():
            for perm, label in admin_perms.iteritems():
                perms[name+perm] = Permission()
                perms[name+perm].name = name+perm
                perms[name+perm].label = label % (name)

    return perms

@implements('before_request')
def before_request():

    """
    Implementation of hook ``before_request``.
    """

    if g.dbcon:
        invoke('user_load_current')


@implements('user_load_current')
def user_load_current():

    """
    Implementation of hook ``user_load_current``.
    """

    if session.has_key('uid'):
        try:
            g.user =  User(session['uid'])
        except LookupError:
            flash('Your user account seems to have gone missing. Please log in again.')
            g.user = User(0)
    else:
        g.user = User(0)



@implements('menu_admin')
def menu_admin(menu):

    """
    Implementation of hook ``menu_admin``.

    ..  todo::

        Make menu stuff be not retarded.
    """

    menu.add('/admin/user', 'User administration')
    menu.add('/admin/user/add', 'Add user')
    menu.add('/admin/group', 'Group administration')
    menu.add('/admin/group/add', 'Add group')


@implements('form_install')
def form_install(form):

    """
    Implementation of hook ``form_install``.

    Extends the installation form.
    """

    form.username = TextField('User', validators=[Required()])
    form.password = PasswordField('Password', validators=[Required(), EqualTo('password_confirm', message="Passwords don't match")])
    form.password_confirm = PasswordField(u'Password confirmation', validators=[Required()])


@implements('form_install_submit')
def form_install_submit(form):

    """
    Implementation of hook ``form_install_submit``.

    Submit handler for the installation form.
    """

    #add perms
    admin_perms = odict()
    all_perms = invoke('permission_build')

    for mod, perms in all_perms.iteritems():
        for name, perm in perms.iteritems():
            perm.save()
            perm.value = True
            admin_perms[name] = perm


    #add admin  group
    admins = Group()
    admins.name = 'admin'
    admins.label = 'Administrators'
    admins.perms = admin_perms
  
    admins.save()


    admin = User()
    admin.name = form.username.data
    admin.active = True
    admin.password = form.password.data

    admin.groups = {
        'admins': admins
    }

    admin.save()


    anon = User()
    anon.id = 0
    anon.name = 'Anon'
    anon.active = True
    anon.password = ''.join(random.choice(string.ascii_letters + string.digits) for x in range(64))

    anon.save()



#form element constructors

@register_form_constructor(str)
@register_form_constructor(unicode)
def element_string(name):

    """
    Form constructor for field type ```str``.
    """

    return TextField(name)


@register_form_constructor(int)
def element_int(name):

    """
    Form constructor for field type ``int``.
    """

    return TextField(name)


@register_form_constructor(bool)
def element_bool(name):

    """
    Form constructor for field type ``bool``.
    """

    return BooleanField(name)


@register_form_constructor(Permission, multi=True)
def element_Permission(name):

    """
    Multi Form constructor for field type ``Permission``.
    """

    perms = perms_all()
    try:

        class f(Form):
            pass

        for pname, perm in perms.iteritems():
            setattr(f, pname, PermissionField(perm.label, min=-1, max=1))

        return FormField(f)
        #return TextField('nope')

    except Exception as e:
        print "_______EXCEPTION______ :", e
        raise


@register_form_constructor(Group, multi=True)
def element_Group(name):

    """
    Multi Form constructor for field type ``Group``.
    """
   
    groups = groups_list()

    choices = []
    for k, v in groups.iteritems():
        choices.append((k, v))

    return GroupField(name, choices=choices)


#form element handlers

@register_form_handler(str)
@register_form_handler(unicode)
def handle_str(name, data):

    """
    Form handler for field type ``str``.
    """

    try:
        value = data[name]
    except: raise

    return value


@register_form_handler(bool)
def handle_bool(name, data):

    """
    Form handler for field type ``bool``.
    """

    try:
        value = bool(data[name])
    except: raise

    return value


@register_form_handler(Group, multi=True)
def handle_Group(name, data):

    """
    Form handler for field type ``Group``.
    """


    try:

        groups = odict()
        keys = data.getlist(name)

        for gname in keys:
            groups[gname] = Group(gname)

    except: raise

    return groups


@register_form_handler(Permission, multi=True)
def handle_Permission(name, data):

    """
    form handler for field type ``Permission``.
    """

    try:
        
        perms = odict()
        keys = data.keys()

        for key in keys:
            
            if key.startswith("%s-" % name) and key != ("%s-csrf_token" % name) and data[key] != u'0':

                try:
                    val = int(data[key])

                except ValueError:
                    print "something went wonky, get debugging shit done"

                pname = key.lstrip("%s-" % name)
                perm = Permission(pname)
                perm.value = bool(val+1) # -1 = False, 1 = True;

                perms[pname] = perm

    except: raise

    return perms


#field renderers

@register_field_renderer(str)
@register_field_renderer(unicode)
def render_str(obj):

    """
    Field renderer for field type ``str``.
    """

    try:
        r = value(obj.val)
    except: raise

    return r


@register_field_renderer(bool)
def render_bool(obj):

    """
    Field renderer for field type ``bool``.
    """

    try:
        r = 'Yes' if obj.val else 'No'
    except: raise

    return r


@register_field_renderer(Group, multi=True)
def render_Group(obj):

    """
    Field renderer for field type ``Group``.

    ..todo: Handle single Group
    """

    groups = []

    if obj.multi:
        for name, group in obj.val.iteritems():
            groups.append(link(url_for('page_admin_edit', mod='common', cls='Group', id=group.name), group.label))

    return ulist(groups)


@register_field_renderer(Permission, multi=True)
def render_Permission(obj):

    """
    Field renderer for field type ``Permission``.

    ..todo: Handle single Permission
    """

    permissions = []

    if obj.multi:
        for name, perm in obj.val.iteritems():
            caption = "%s: %s" % ('GRANT' if perm.value else 'DENY', perm.label)
            permissions.append(link(url_for('page_admin_edit', mod='common', cls='Permission', id=perm.name), caption))

    return ulist(permissions)




def render_name(mod, cls, field):

    if type(field.parent_id) is int:
        url = "/admin/%s/%s/%d" % (mod, cls, field.parent_id)
        return link(url, field.val)
    else:
        return value(field.val)


#Forms below this


class FormLogin(Form):

    """
    Login Form.
    """

    name = TextField('User', validators=[Required()])
    password = PasswordField('Password', validators=[Required()])



# page handlers below this

@app.route('/admin')
@access('admin_view')
@view
def page_admin():

    """
    Page callback: /admin

    Shows an overview of all auto-administered modules and classes.
    """

    return value('admin placeholder')



@app.route('/admin/<mod>')
@access('admin_view')
@view
def page_admin_module(mod):

    """
    Page callback: /admin/<mod>

    Shows an overview of all auto-administered classes for module ``mod``
    """

    if administered.has_key(mod):
        
        items = []
        for k, v in administered[mod].iteritems():
            items.append(link('/admin/'+mod+'/'+k, k))


        return ulist(items)
    else:
        abort(404, 'Unknown module')
  


@app.route('/admin/<mod>/<cls>/')
@access('admin_view')
@view
def page_admin_class(mod, cls):
   
    """
    Page callback: /admin/<mod>/<cls>

    Shows a list of all items belonging to class ``cls`` in module ``mod``.
    """

    if not (administered.has_key(mod) and administered[mod].has_key(cls)):

        abort(404, 'Unknown module and/or class')

    else:

        clsobj = administered[mod][cls]

        if hasattr(clsobj, '_id'):
            key = clsobj._id 
        else:
            key = 'id'

        rows = db_query("SELECT %s FROM `%s` ORDER BY %s ASC" % (key, cls.lower(), key))

        if len(rows) > 0:
            objs = odict()
            for row in rows:
                objs[row[key]] = clsobj(row[key])

            t = odict()
            t['head'] = odict()
            t['rows'] = odict()

            for name, field in objs[rows[0][key]].__fields__.iteritems():
                v = field.render()

                if v is not False:
                    if hasattr(field, 'label'):
                        t['head'][name] = unicode(field.label)
                    else:
                        t['head'][name] = name

            t['head']['edit'] = value('Edit')
            t['head']['delete'] = value('Delete')


            for oid, obj in objs.iteritems():
                row = odict()
                for k, field in obj.__fields__.iteritems():

                    r = field.render()
                    if r is not False:
                        row[k] = r

                row['edit'] = link(url_for('page_admin_edit', mod=mod, cls=cls, id=oid), 'Edit')
                row['delete'] = link(url_for('page_admin_delete', mod=mod, cls=cls, id=oid), 'Delete')

                t['rows'][oid] = row

            return table(t, id=str(cls).lower()+'-list')


        else:
            
            return value("No entities of type '%s' in database." % str(cls))



@app.route('/admin/<mod>/<cls>/add')
@access('add', callback=access_admin)
@view
def page_admin_add(mod, cls):

    """
    Page callback: /admin/<mod>/<cls>/add

    Shows the administration form for a particular class ``cls`` in a particular
    module ``mod``.

    ..  todo::

        Make this handle /admin/<mod>/<cls>/edit as well. Check if that makes
        sense first!
    """

    if not administered.has_key(mod) and administered[mod].has_key(cls):

        abort(404, 'Unknown module and/or class')

    else:
        clsobj = administered[mod][cls]
        obj = clsobj()

        formclass = obj.getform()

        f = formclass(request.form, clsobj)
        e = form_process(f)
        return form(e)



@app.route('/admin/<mod>/<cls>/add', methods=['POST'])
@access('add', callback=access_admin)
def page_admin_add_submit(mod, cls):

    """
    POST callback: /admin/<mod>/<cls>/add

    Handles the submission of the administration form belonging to the class
    ``cls`` in module ``mod``.

    ..  todo::

        Make this handle /admin/<mod>/<cls>/edit as well. Check if that makes
        sense first!
    """

    if administered.has_key(mod) and administered[mod].has_key(cls):


        clsobj = administered[mod][cls]
        fields = clsobj.__fields__

        obj = clsobj()

        for k, v in fields.iteritems():
            handler = form_handlers[v.key]
            setattr(obj, k, handler(k, request.form))

        res = obj.save()
    
        if res.rowcount == 1:
            flash("Successfully added %s %s" % (cls.lower(), obj.identity()))
            return redirect(url_for('page_admin_edit', mod=mod, cls=cls, id=obj.id))
        
        else:
            flash("Could not save %s %s." % (cls.lower(), obj.identity()), 'error')
            return redirect(url_for('page_admin_class', mod=mod, cls=cls))
        
  
    abort(404, 'Unknown module and/or class')



@app.route('/admin/<mod>/<cls>/<id>')
@access('edit', callback=access_admin)
@view
def page_admin_edit(mod, cls, id):

    """
    Page callback: /admin/<mod>/<cls>/<id>

    Shows the administration form for a particular class ``cls`` in a particular
    module ``mod``.
    """
    
    if administered.has_key(mod) and administered[mod].has_key(cls):
        clsobj = administered[mod][cls]
        obj = clsobj(id) 
       
        formclass = obj.getform()

        f = formclass(request.form, obj)
        e = form_process(f)
        return form(e)


    abort(404, 'Unknown module and/or class')



@app.route('/admin/<mod>/<cls>/<id>', methods=['POST'])
@access('edit', callback=access_admin)
def page_admin_edit_submit(mod, cls, id):

    if administered.has_key(mod) and administered[mod].has_key(cls):

        clsobj = administered[mod][cls]
        fields = clsobj.__fields__
        obj = clsobj(id)

        for k, v in fields.iteritems():
            handler = form_handlers[v.key]
            setattr(obj, k, handler(k, request.form))

        res = obj.save()

        if res.rowcount == 1:
            flash("Successfully updated %s %s" % (cls.lower(), obj.identity()))
            return redirect(url_for('page_admin_edit', mod=mod, cls=cls, id=id))

        else:
            flash('Could not save user.', 'error')
            return redirect(url_for('page_admin_class', mod=mod, cls=cls))

    abort(505, 'Unknown module and/or class.')



@app.route('/admin/<mod>/<cls>/<id>/delete')
@access('delete', callback=access_admin)
@view
def page_admin_delete(mod, cls, id):

    if administered.has_key(mod) and administered[mod].has_key(cls):
        
        content = odict()

        clsobj = administered[mod][cls]
        obj = clsobj(id)

        content['info'] = value("You are about to delete the %s %s." % (cls.lower(), obj.identity()))
        content['warning'] = value('This cannot be undone, continue?')

        class FormDelete(Form):
            submit = SubmitField('Yes, delete')

        content['form'] = form(form_process(FormDelete()))

        return content

    abort(505, 'Unknown module and/or class.')



@app.route('/admin/<mod>/<cls>/<id>/delete', methods=['POST'])
@app.route('/admin/<mod>/<cls>/<id>', methods=['DELETE'])
@access('delete', callback=access_admin)
def page_admin_delete_submit(mod, cls, id):

    if administered.has_key(mod) and administered[mod].has_key(cls):

        clsobj = administered[mod][cls]
        obj = clsobj(id)
        
        identity = obj.identity()

        if(obj.delete().rowcount == 1):
            flash("%s %s successfully deleted." % (cls.lower(), identity))
            return redirect(url_for('page_admin_class', mod=mod, cls=cls))

        else:
            flash("Could not delete %s %s, try again" % cls.lower, identity)
            return redirect(url_for('page_admin_delete', mod=mod, cls=cls, id=id))

    else:
        abort(505, 'Unknown module and/or class.')



@app.route('/login')
@view
def page_login():

    """
    Page callback: /login

    Either shows the login, or who you're logged in as.

    ..  todo::

        Make this redirect to the user profile if logged in.
    """

    if g.user.id is not 0:
        return value('Logged in as '+g.user.name)
        
    else:

        r = invoke('form_login', FormLogin)
        FormLogin.submit = SubmitField('Login')

        f = FormLogin(request.form)
        e = form_process(f) 

        return form(e)
        #content_set(form(e))
        #return render()



@app.route('/login', methods=['POST'])
def page_login_submit():

    """
    POST callback: /login

    Logs the user in or displays login error message.
    """

    u = login(request.form['name'], request.form['password'])

    if u.id is not 0:
        flash('Welcome, '+u.name+'.')

        if session.has_key('redirect'):
            url = session['redirect']
            del session['redirect']
        else:
            url = url_for('frontpage')

        return redirect(url)

    else:
        flash('Wrong username or password.')
        return redirect(url_for('page_login'))



@app.route('/logout')
def page_logout():

    """
    Page callback: /logout

    Logs the user out and redirects to the frontpage.
    """

    if g.user.id is not 0:
        del session['uid']
        flash('Goodbye, '+g.user.name+'.')

    return redirect(url_for('frontpage'))

