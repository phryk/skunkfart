"""
Element shortcut module

..  todo::

    Create a class for the ``value`` variable to avoid issues with same
    property and key names (as with ``items``).
"""

from collections import OrderedDict as odict


def element(type, value, **kwargs):

    for key, val in kwargs.iteritems():
        if str(key).startswith('_'):
            key = key[1:]

        value[key] = val

    return  {
        'value': value,
        'template': type+'.jinja'
    }


def value(value):
    return element('value', value)


def text(value, **kwargs):

    v = odict()
    v['text'] = value

    return element('text', v, **kwargs)


def container(value, **kwargs):

    v = odict()
    if not type(value) is list:
        value = [value]

    v['children'] = value

    return element('container', v, **kwargs)


def link(url, label, **kwargs):

    value = {
        'url': url,
        'label': label,
    }

    return element('link', value, **kwargs)


def table(value, **kwargs):

    ldict = lambda x: odict(zip(range(0, len(x)), x))

    if value.has_key('head') and not hasattr(value['head'], 'itervalues'):
        value['head'] = ldict(value['head'])

    if value.has_key('rows') and not hasattr(value['rows'], 'itervalues'):
        value['rows'] = ldict(value['rows'])

    if value.has_key('row'):
        for k, row in value['rows'].itervalues():
            if not hasattr(row, 'itervalues'):
                value['rows'][k] = ldict(row)

    return element('table', value, **kwargs)


def form(value, **kwargs):
    return element('form', value, **kwargs)


def ulist(items, **kwargs):

    value = {
        'items': items
    }

    return element('ulist', value, **kwargs)
