"""
phryk.net blog thingie
"""

from functools import wraps
from os.path import exists
from collections import OrderedDict as odict
from itertools import chain
from contextlib import closing
import sqlite3
from flask import Flask, request, session, render_template, g, redirect, flash, abort
from flask.ext.wtf import Form, SubmitField

class Skunkfart(Flask):

    def __init__(self, *args, **kwargs):

        super(Skunkfart, self).__init__(*args, **kwargs)

        if not self.config.has_key('jinja_autoescape'):
            self.config['jinja_autoescape'] = ('.jinja')


    def select_jinja_autoescape(self, filename):

        if filename is None:
            return False

        return filename.endswith(self.config['jinja_autoescape'])



app = Skunkfart(__name__)

import config
import mod

from elements import *


modules = {}


# custom exceptions below this

class DatabaseError(Exception):
    pass


# auxiliary functions below this

def render(page = None, template = 'index'):

    if page is None:
        page = g.page

    templates = [
        app.config['theme']+'/'+template+'.jinja',
        'default/'+template+'.jinja'
    ]

    page['regions'] = template_process(page['regions'])
    return render_template(templates, page=page)



def region_add(name):
    if not g.page['regions'].has_key(name):
        g.page['regions'][name] = {}


def regions_build():

    #add a few standard regions we can rely upon to exist
    region_add('content')
    region_add('messages')

    for region in app.config['regions'].values():
        region_add(region)


def template_process(item):
    if type(item) is list:
      for i, subitem in enumerate(item):
        item[i] = template_process(subitem)
    if type(item) is dict or type(item) is odict:
        for key, subitem in item.iteritems():
            if key is 'template':
                item['template'] = template_splice(subitem)
            else:
                item[key] = template_process(subitem)

    return item


def template_splice(template):
    if(type(template) is list):
        templates = []
        for t in template:
            templates.append(template_splice(t))

        return list(chain.from_iterable(templates))
        
    else:
        return [
            app.config['theme']+'/'+template,
            'default/'+template
        ]


def title_append(x):
    g.page['title'] = g.page['title'] + app.config['title_separator'] + x
    return g.page['title']


def block_set(region, block, value):
    g.page['regions'][region][block] = value


def content_set(value):
    block_set('content', 'content', value)


def db_connect(force = False):
    if exists(app.config['database']) or force:
        return sqlite3.connect(app.config['database'])
    else:
        raise DatabaseError("Database doesn't exist.")


def db_close():
    if hasattr(g, 'dbcon') and g.dbcon:
        g.dbcon.close()


def db_init():
    with closing(db_connect(True)) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()



# Shit my brain can't even parse this, shamelessly copied from http://flask.pocoo.org/docs/patterns/sqlite3/
def db_query(query, args=(), one=False):
    cur = g.dbcon.execute(query, args)
    rv = [dict((cur.description[idx][0], value)
        for idx, value in enumerate(row)) for row in cur.fetchall()]

    return (rv[0] if rv else None) if one else rv


def db_commit():
    g.dbcon.commit()


def form_process(form, id = None, method = 'POST', action = None, enctype = None):

    #modifications by hooks
    hook = form.__class__.__name__ + '_alter'
    hr = mod.invoke(hook, form)
   
    f = {}
    f['id'] = id if id else form.__class__.__name__
    f['method'] = method
    f['action'] = action if action else request.path
    
    if enctype:
        f['enctype'] = enctype

    
    fields = odict()
    for elem in form:
        fields[elem.name] = {
            'template': ['form-item-'+elem.type+'.jinja', 'form-item-default.jinja'],
            'value' : elem
        }

    f['fields'] = fields
    
    return f


def fatal(code, error):

    g.page['regions']['content']['fatal'] = {
        'value': {
            'code': code,
            'message': str(error),
        },
        'template': 'error.jinja'
    }
    return render(), code



# decorators below this

def view(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        content_set(f(*args, **kwargs))
        return render(g.page)

    return wrapper



# hooks'n'stuff

@app.before_first_request
def boot():
   mod.invoke('boot') 


@app.before_request
def request_init():

    db_blacklist_urls = [
        '/install',
        '/favicon.ico'
    ]

    g.page = {}
    g.page['title'] = app.config['title'];
    g.page['regions'] = {}
    regions_build()

    
    if request.path not in db_blacklist_urls:
    
        try:
          g.dbcon = db_connect()
        except:
          raise
    
        mod.invoke('before_request')

    mod.invoke('regions_populate') 


@app.teardown_request
def request_teardown(exception):
    db_close()



# page views

@app.route('/')
@view
def frontpage():
    return "frontpage"


@app.route('/install', methods=['GET', 'POST'])
def page_install():

    class FormInstall(Form):
        pass


    title_append('Installation')

    if not exists(app.config['database']):


        mod.invoke('form_install', FormInstall)
        FormInstall.submit = SubmitField(u'Install')
        f = FormInstall(request.form)
        e = form_process(f)

        if request.method == 'POST' and f.validate():
            db_init()
            g.dbcon = db_connect()
            r = mod.invoke('form_install_submit', f)
            flash('Installation procedure executed')
            flash('Result of hook-invokation: '+str(r))
            return redirect('/')
        elif request.method == 'POST':
            flash('form input not valid')

        content_set(form(e))
        
    else:
        flash('Not installing, database already exists.')
        #die('wtf')

    return render()



#error handlers after this

@app.errorhandler(401)
def e401(error):
    if g.user.id == 0:
        session['redirect'] = request.path
        flash('Access denied. Log in to proceed.')
        return redirect('/login')
    return fatal(401, error)


@app.errorhandler(404)
def e404(error):
    return fatal(404, error)


@app.errorhandler(500)
def e500(error):
    return fatal(500, error)


@app.errorhandler(501)
def e501(error):
    return fatal(501, error)


# Make page views throwing exceptions look pretty if DEBUG isn't enabled
if not app.config['DEBUG']:
    @app.errorhandler(Exception)
    @view
    def e_exception(e):

        content = odict()

        error = []
        error.append(text('Caught an exception.', _class='error'))
        error.append(
            text(
              [text("%s" % e.__class__.__name__, _class='exception-type'),
               text("%s" % unicode(e.message), _class='exception-message')],
              _class='exception'))

        content['exception'] = container(error, _class='error-wrapper')

        return content






#discover and import modules
mod.load()
mod.implementations_order()
