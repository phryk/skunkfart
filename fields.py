from flask.ext.wtf import Field, Input

_unset_value = object()

class RangeWidget(Input):

    input_type = 'range'

    def __init__(self, min=0, max=10):
        self.min = min
        self.max = max
        super (RangeWidget, self).__init__()

    def html_params(self, **kwargs):
        if not kwargs.has_key('min'):
            kwargs['min'] = self.min

        if not kwargs.has_key('max'):
            kwargs['max'] = self.max

        return super(RangeWidget, self).html_params(**kwargs)


class RangeField(Field):

    """
    RangeField thingie

    ..todo:: implement __init__ in order to have dynamic min and max values
    """

    def __init__(self, label, min=0, max=10, **kwargs):

        if not 'widget' in kwargs:
            self.widget = RangeWidget(min=min, max=max)

        super(RangeField, self).__init__(label, **kwargs)

    def _value(self, *args, **kwargs):

        return self.data
