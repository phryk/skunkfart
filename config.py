from skunkfart import app
from flask import config

app.secret_key = "foobar"

app.config['DEBUG'] = True
app.config['theme'] = 'phryk'

app.config['database'] = 'data.db'
app.config['regions'] = {}

app.config['title'] = 'skunkfart'
app.config['separator'] = ' | '
app.config['title_separator'] = app.config['separator']
