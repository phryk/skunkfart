import os
from collections import OrderedDict as odict
from flask import g
import skunkfart


implementations = odict() #data structure holding all hook implementations with their dependencies


def discover():
    
    rootdir = os.path.dirname(__file__)
    
    modules = []
    for filename in os.listdir(rootdir+'/modules'):
        if filename != '__init__.py' and filename.endswith('.py'):
            modname = filename.rpartition('.py')[0]
            modules.append(modname)

    return modules


def load():
    for modname in discover():
        skunkfart.modules[modname] = getattr(__import__('modules.'+modname), modname)


# reorder implementations to accomodate hook execution-dependencies
def deps_resolve(imps):
    r = odict() 
    nonmatch = None
    l = len(imps)
    i = 0
    
    while len(imps) and i < l**2:
        for k, imp in imps.iteritems():
            deps_matched = True
            for dep in imp['deps']:
                if not dep in r.keys():
                    nonmatch = dep 
                    deps_matched = False

            if deps_matched:
                r[imp['id']] = imp
                imps.pop(k)
            i = i + 1

        if i > l**2:
            raise LookupError("Going through implementations more than l**2 times ("+str(i)+"), aborted")

    return r

def implementations_order():
    for hook, imps in implementations.iteritems():
        implementations[hook] = deps_resolve(imps)

def implements(hook, deps = []):

    global implementations

    if not implementations.has_key(hook):
        implementations[hook] = odict()

    def decorator(func):
        global implementations

        modname = func.__module__
        modname = modname[8:] #remove initial modules.

        id = modname+'.'+func.__name__
        implementations[hook][id] = {
            'id': id,
            'callback': func,
            'deps': deps
        }

        #implementations[hook] = sort_deps(implementations[hook], hook)

        return func

    return decorator

def invoke(hook, *args, **kwargs):
    ret = {}
    if implementations.has_key(hook):
        for id, impl in implementations[hook].iteritems():
            ret[impl['id']] = impl['callback'](*args, **kwargs)

    return ret
