.. skunkfart documentation master file, created by
   sphinx-quickstart on Tue Jan 15 22:04:57 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to skunkfart's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: skunkfart
    :members:

.. automodule:: modules.common
    :members:
    :special-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

